<?php

session_start();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // DATOS CAPTURADOS DEL FORM
    $data = array(
        'email' => htmlspecialchars($_POST['email'] ?? null),
        'password' => htmlspecialchars($_POST['password'] ?? null)
    );

    /*  // Array de inputs incorrectos
    $wrong_inputs = array(); */

    //SAL & MD5 HASH
    $salt = 'XyZzy12*_';
    $md5 = hash('md5', $salt . $data['password']);

    // STORED_HASH de la BD
    $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

    // VALIDACIONES

    if (empty($data['email']) || empty($data['password'])) {

        $_SESSION['error'] = 'Se require email y contraseña';
        header('Location: login.php');
        return;
    } elseif ($md5 != $stored_hash) {

        // ERROR LOGGING
        error_log('Login fail' . $_POST['email'] . $md5);

        $_SESSION['error'] = 'La contraseña es incorrecta';

        header('Location: login.php');
        return;
    } elseif (!strpos($data['email'], '@')) {

        $_SESSION['error']  = 'El email debe incluir un signo (@)';
        header('Location: login.php');
        return;
    } elseif (isset($md5) && $md5 === $stored_hash) {

        // SUCCES LOGGING
        error_log('Login success' . $_POST['email']);

        $_SESSION['name'] = $_POST['email'];

        header("Location:autos.php") and die();
    }
}

require './views/login.view.php';
