<?php
require_once './lib/Database.php';

//MODELO DE AUTO
class Auto
{


    private $db;

    // Campos del modelo/tabla
    private $make;

    private $year;

    private $mileage;

    //Constructor

    public function __construct($make = "", $year = 0, $mileage = 0)
    {

        $this->make = $make;
        $this->year = $year;
        $this->mileage = $mileage;
    }

    // CONEXION ////////////////////////////////////////////

    public function makeConexion()
    {

        $this->db = new Database();
    }

    // GETTETS Y SETTERS//////////////////////

    /**
     * Get the value of make
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set the value of make
     */
    public function setMake($make): self
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get the value of year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     */
    public function setYear($year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of mileage
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set the value of mileage
     */
    public function setMileage($mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }


    // CUSTOM METHODS /////////////////////////////////

    // Get Autos

    public function getAutos()
    {


        $this->db->query("SELECT * from autos");

        $results = $this->db->getAllResults('Auto');

        return $results;
    }

    // Get one auto

    public function getOneAuto($id)
    {

        $this->db->query("SELECT * from autos WHERE auto_id= :id");

        $this->db->bind(':id', $id);

        $result = $this->db->getOneResult('Auto');

        return $result;
    }

    // Update Autos

    public function upDateAuto($auto, $id)
    {

        $this->db->query("UPDATE autos SET make=:mk, year=:yr, mileage=:mi WHERE auto_id=:id");

        $this->db->bind(':mk', $auto->make);
        $this->db->bind(':yr', $auto->year);
        $this->db->bind(':mi', $auto->mileage);
        $this->db->bind(':id', $id);

        $this->db->execute();
    }

    // Insert autos

    public function addAuto($auto)
    {

        // QUERY del insert con parametros
        $this->db->query('INSERT into autos(make, year, mileage) values(:mk, :yr, :mi)');


        // BIND de los parámetros del query usando los métodos propios de la clase Auto para acceder a las propiedades privadas. 
        $this->db->bind(':mk', $auto->make);
        $this->db->bind(':yr', $auto->year);
        $this->db->bind(':mi', $auto->mileage);

        //EXECUTE de la query
        $this->db->execute();
    }

    //Delete autos // PREGUNTAR LO DEL BIND al $id

    public function delAuto($id)
    {

        //QUERY
        $this->db->query("DELETE from autos WHERE auto_id = :id");

        //BIND 
        $this->db->bind(':id', $id);

        //EXECUTE
        $this->db->execute();
    }

    //Cuenta registros

    public function getNumRows()
    {

        return $this->db->getRows('autos');
    }
}
