<?php
session_start();

require_once './models/Auto.php';
$car = new Auto();
$car->makeConexion();

if (!isset($_SESSION['name'])) {

    die('ACCESO DENEGADO');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $success_message = null;


    if (isset($_POST['logout'])) {

        header('Location: logout.php') and die();
    } elseif (isset($_POST['auto_update'])) {

        header("Location:autos.update.php?id=" . urlencode($_POST['auto_update'])) and die();
    } else {

        header("Location:autos.add.php") and die();
    }
}





require_once './views/autos.view.php';
