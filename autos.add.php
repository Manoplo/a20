<?php
session_start();

require_once './models/Auto.php';
$car = new Auto();
$car->makeConexion();

if (!isset($_SESSION['name'])) {

    die('ACCESO DENEGADO');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $success_message = null;


    if (isset($_POST['cancel'])) {
        header('Location:autos.php') and die();
    } elseif (isset($_POST['add'])) {

        // CAPTURA LAS VARIABLES

        $make = htmlspecialchars($_POST['make']);
        $year = htmlspecialchars($_POST['year']);
        $mileage = htmlspecialchars($_POST['mileage']);



        $errors = array();

        //VALIDACIONES

        if (!$make) {

            $errors['no_make'] = 'El campo marca es obligatorio';
        }

        if (isset($year) && !is_numeric($year) || isset($mileage) && !is_numeric($mileage)) {

            $errors['not_numeric'] = 'Los campos año y kilometraje han de ser numéricos';
        }

        if (empty($errors)) {

            try {

                // Instancia 
                $newCar = new Auto($make, $year, $mileage);
                $car->addAuto($newCar);
                $_SESSION['success'] = "Registro insertado correctamente";
                header('Location: autos.php');
                return;

                /*    /* $sql = 'INSERT INTO autos(make, year, mileage) VALUES(:mk, :yr, :mi)';
                $stmt = $pdo->prepare($sql);
                $stmt->execute(array(':mk' => $make, ':yr' => $year, ':mi' => $mileage)); */
            } catch (PDOException $th) {

                echo $th;
            }
        }
    }
}

require_once './views/autos.add.view.php';
