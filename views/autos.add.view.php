<?php require 'partials/head.part.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-5">Añada un vehículo:</h1>
            <form method="POST" action="autos.add.php">
                <div class="mb-3">
                    <label for="email" class="form-label">Marca</label>
                    <input type="text" class="form-control" name="make" id="make">

                    <?php if (isset($errors['no_make'])) : ?>
                        <p class="text-danger"><?= $errors['no_make'] ?></p>
                    <?php endif; ?>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Año</label>
                    <input type="text" name="year" id="year" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Kilometraje</label>
                    <input type="text" name="mileage" id="mileage" class="form-control">
                </div>
                <?php if (isset($errors['not_numeric'])) : ?>
                    <p class="text-danger"><?= $errors['not_numeric'] ?></p>
                <?php endif; ?>
                <button type="submit" class="btn btn-success" name="add" value="add">Añadir</button>
                <button type="submit" class="btn btn-danger" name="cancel" value="cancel">Cancel</button>
            </form>
        </div>
        <div class="col-6">
            <div class="mt-5">
                <h5 class="d-flex justify-content-end">Usuario:</h5>
                <p class="d-flex justify-content-end"><?php echo $_SESSION['name'] ?></p>
            </div>
        </div>

        <?php require 'partials/foot.part.php'; ?>