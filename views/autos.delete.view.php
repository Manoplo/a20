<?php require 'partials/head.part.php'; ?>
<div class="container">

    <div class="row">
        <div class="col-6">
            <div class="col-3 mt-5">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title"><?= $newAuto->getMake(); ?></h4>
                        <h5>Año:</h5>
                        <p class="card-text"><?= $newAuto->getYear(); ?></p>
                        <h5>Kilómetros:</h5>
                        <p class="card-text"><?= $newAuto->getMileage(); ?></p>
                    </div>
                </div>
            </div>
            <form method="POST">
                <input type="hidden" name="auto_del" value="<?= $id; ?>">
                <button type="submit" name="deletebtn" value="delete" class="btn btn-success">Borrar</button>
                <button type="submit" name="cancelbtn" value="cancel" class="btn btn-danger">Cancel</button>
            </form>
        </div>
        <div class="col-6">
            <div class="mt-5">
                <h5 class="d-flex justify-content-end">Usuario:</h5>
                <p class="d-flex justify-content-end"><?php echo $_SESSION['name'] ?></p>
            </div>
        </div>
    </div>
</div>

<?php require 'partials/foot.part.php'; ?>