<?php require 'partials/head.part.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-5">Actualice el registro:</h1>
            <form method="POST">
                <div class="mb-3">
                    <label for="make" class="form-label">Marca</label>
                    <input type="text" class="form-control" name="make" id="make" value="<?= $car->getMake(); ?>">

                </div>
                <div class="mb-3">
                    <label for="year" class="form-label">Año</label>
                    <input type="text" name="year" id="year" class="form-control" value="<?= $car->getYear(); ?>">
                </div>
                <div class="mb-3">
                    <label for="mileage" class="form-label">Kilometraje</label>
                    <input type="text" name="mileage" id="mileage" class="form-control" value="<?= $car->getMileage(); ?>">
                    <?php if (isset($_SESSION['error'])) : ?>
                        <p style="color:red"> <?= $_SESSION['error']; ?></p>
                        <?php unset($_SESSION['error']); ?>

                    <?php endif ?>
                </div>
                <button type="submit" class="btn btn-success" name="update" value="update">Actualizar</button>
                <button type="submit" class="btn btn-dark" name="logout" value="logout">Cancelar</button>
            </form>
        </div>
        <div class="col-6">
            <div class="mt-5">
                <h5 class="d-flex justify-content-end">Usuario:</h5>
                <p class="d-flex justify-content-end"><?php echo $_SESSION['name'] ?></p>
            </div>
        </div>
    </div>
</div>

<?php require 'partials/foot.part.php'; ?>