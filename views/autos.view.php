<?php require 'partials/head.part.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-5">Autos.DataBase</h1>

            <form method="POST" action="autos.php">
                <button type="submit" class="btn btn-success">Añadir</button>
                <button type="submit" class="btn btn-dark" name="logout" value="logout">Log-out</button>
            </form>
            <?php if (isset($_SESSION['success'])) : ?>
                <div class="alert alert-success mt-3" role="alert">
                    <?= $_SESSION['success']; ?>
                    <?php unset($_SESSION['success']); ?>
                </div>
            <?php endif ?>
        </div>
        <div class="col-6">
            <div class="mt-5">
                <h5 class="d-flex justify-content-end">Usuario:</h5>
                <p class="d-flex justify-content-end"><?php echo $_SESSION['name'] ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr class="mt-4 col-6" />
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <h2 class="mt-5">Nuestros vehículos:</h2>
                <?php if ($car->getNumRows() < 1) : ?>
                    <p>No se encontraron registros</p>
                <?php else : ?>
                    <p>Encontrados <?= $car->getNumRows(); ?> registros</p>
                <?php endif ?>
            </div>
        </div>
        <div class="row">

            <?php $allcars = $car->getAutos();
            ?>


            <?php foreach ($allcars as $auto) : ?>
                <div class="col-3">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h4 class="card-title"><?= $auto->getMake(); ?></h4>
                            <h5>Año:</h5>
                            <p class="card-text"><?= $auto->getYear(); ?></p>
                            <h5>Kilómetros:</h5>
                            <p class="card-text"><?= $auto->getMileage(); ?></p>
                            <form method="POST" action="autos.delete.php">
                                <input type="hidden" name="auto_del" value="<?= $auto->auto_id; ?>">
                                <button type="submit" class="btn btn-danger mb-3">Eliminar</button>
                            </form>
                            <form method="POST">
                                <input type="hidden" name="auto_update" value="<?= $auto->auto_id; ?>">
                                <button type="submit" class="btn btn-success">Actualizar</button>
                            </form>

                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>





<?php require 'partials/foot.part.php'; ?>