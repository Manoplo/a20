<?php require_once 'partials/head.part.php';?>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-5">Por favor, introduzca sus datos de acceso:</h1>
            <form method="POST" action="login.php">
                <div class="mb-3">

                    <label for="email" class="form-label">Dirección de Email</label>
                    <input type="text" class="form-control" name="email" id="email">

                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Contraseña</label>
                    <input type="password" name="password" id="password" class="form-control">

                    <?php if(isset($_SESSION['error'])): ?>
                    <p class="text-danger"><?= $_SESSION['error']; ?></p>
                    <?php unset($_SESSION['error']); ?>
                    <?php endif ?>

                </div>
                <button type="submit" class="btn btn-dark">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php require_once 'partials/foot.part.php'; ?>