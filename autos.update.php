<?php
session_start();

require_once './models/Auto.php';

$auto = new Auto();
$auto->makeConexion();



if (!isset($_SESSION['name'])) {
    die('ACCESO DENEGADO');
}

// GET ID
$id = htmlspecialchars($_GET['id']) ?? null;

// FETCH ROW

$car = $auto->getOneAuto($id);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    //Logout

    if (isset($_POST['logout'])) {

        header('Location:autos.php') and die();
    } elseif (isset($_POST['update'])) {

        // CAPTURO VARIABLES

        $make = htmlspecialchars($_POST['make']) ?? null;
        $year = htmlspecialchars($_POST['year']) ?? null;
        $mileage = htmlspecialchars($_POST['mileage']) ?? null;

        // UPDATE QUERY

        //VALIDACIONES

        if (!$make) {

            
            $_SESSION['error'] = 'El campo marca es obligatorio';
            header('Location: autos.update.php?id='.urlencode($id));
            return;
        }

        if (isset($year) && !is_numeric($year) || isset($mileage) && !is_numeric($mileage)) {

            $_SESSION['error'] = 'Los campos año y kilometraje han de ser numéricos';
            header('Location: autos.update.php?id='.urlencode($id));
            return;
        }

        if (!isset($_SESSION['error'])) {

            try {

                $newAuto = new Auto($make, $year, $mileage);
                $auto->upDateAuto($newAuto, $id);
                $_SESSION['success'] = "Registro actualizado correctamente";
                header('Location:autos.php');
                return;
            } catch (\PDOException $th) {

                throw $th;
            }
        }
    }
}


require './views/autos.update.view.php';
