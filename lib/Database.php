<?php

require_once './config/config.php';

class Database
{

    // CONEX PARAMS
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    // PROPERTIES

    // dbh es el objeto PDO sobre el que se trabaja
    private $dbh;
    // stmt es la sentencia
    private $stmt;
    // error para las excepciones
    private $error;

    // Constructor

    public function __construct()
    {
        // CONNECTION STRING
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // OPTIONS DEL OBJETO PDO

        $options = [

            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

        ];

        // Construimos un objeto PDO y le pasamos por parámetros del constructor el dsn y las options. 

        try {

            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e) {

            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    //Función para hacer el query que se le pasa por parámetros a la función y que se almacena en la variable stmt

    public function query($sql)
    {

        $this->stmt = $this->dbh->prepare($sql);
    }

    //Función para hacer el bindparams al stmt

    public function bind($param, $value, $type = null)
    {

        // Si el $type es null (es decir, si no se introduce un $type al llamar al método)

        if (is_null($type)) {

            switch (true) {

                case is_int($value):

                    $type = PDO::PARAM_INT;

                    break;

                case is_bool($value):

                    $type = PDO::PARAM_BOOL;

                    break;

                case is_null($value):

                    $type = PDO::PARAM_NULL;

                    break;

                default:

                    $type = PDO::PARAM_STR;

                    break;
            }
        }

        // Bindeamos los valores. 
        $this->stmt->bindValue($param, $value, $type);
    }

    //Execute del stmt.
    public function execute()
    {

        return $this->stmt->execute();
    }

    // Retornamos todos los resultados del execute convertidos en Objetos

    public function getAllResults($model)
    {

        // LLama al propio método execute
        $this->execute();

        // Devuelve todos los resultados convertidos en objetos. 

        return $this->stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);
    }

    public function getOneResult($model)
    {
        //Llama al propio método execute
        $this->execute();

        // Establece el modo de fetch

        $this->stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);

        return $this->stmt->fetch();
    }

    public function getRows($auto)
    {

        $numRows = $this->dbh->query("SELECT COUNT(*) from $auto");
        $totalNumRows = $numRows->fetchColumn();
        return $totalNumRows;
    }
}
