<?php

session_start();
require_once './models/Auto.php';
$auto = new Auto();
$auto->makeConexion();


if (!isset($_SESSION['name'])) {

    die('ACCESO DENEGADO');
} 
    

$id = htmlentities($_POST['auto_del']) ?? null;

// Hago fetch del auto pasado por id para pasarlo por la vista
$newAuto = $auto->getOneAuto($id);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    if (isset($_POST['cancelbtn'])) {

        header('Location: autos.php') and die();
    } elseif (isset($_POST['deletebtn'])) {

        try {

            $auto->delAuto($id);
            $_SESSION['success'] = "Registro eliminado correctamente";
            header('Location: autos.php');
            return;
        } catch (PDOException $th) {
            echo $th;
        }
    }
}

require_once './views/autos.delete.view.php';
